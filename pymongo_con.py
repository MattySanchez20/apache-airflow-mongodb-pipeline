from pymongo import MongoClient
from airflow import DAG

import requests
from airflow.operators.python import PythonOperator
from datetime import timedelta

from datetime import datetime

# Set default_args dictionary
default_args = {
    "owner": "me", # setting the owner of the DAG
    "start_date": datetime.now(), # start date is set to today
    "depends_on_past": False,
    "retries": 0, # retries are disallowed
    "retry_delay": timedelta(minutes=0.1), # if it retries, it waits a tenth of a minute to retry
}

# creating a DAG called MONGO_API that is schedule to repeat everyday
dag = DAG(
    "MONGO_API",
    default_args=default_args,
    schedule_interval=timedelta(days=1)
)

def api_return_request():
    
    meal_list = []

    response = requests.get('http://www.themealdb.com/api/json/v1/1/search.php?f=c') # api request

    meals = response.json()['meals'] # returning meals from API

    for meal in meals: # looping through means returned by API
        
        # obtaining column values
        id_ = meal['idMeal']
        name = meal['strMeal']
        categ = meal['strCategory']
        region = meal['strArea']
        
        temp_dict = { # adding items to a temp dictionary to store data in json-like format for mongoDB
            '_id': id_,
            'name': name,
            'category': categ,
            'region': region
        }
        
        meal_list.append(temp_dict) # inserting dictionary into the menu document
    return meal_list


def request_to_mongo(meal_list):

    db_name = 'meals' # name of database

    collection_name = 'menu' # name of collection within meals database

    client = MongoClient(host='host.docker.internal', port=27017) # connecting client to an internal port within docker where airflow is being run

    client.drop_database(db_name) # makes sure the database is cleared before creating it again

    database = client[db_name] # creating database on mongoDB

    collection = database[collection_name] # creating collection within the database created above

    for meal in meal_list:
        collection.insert_one(meal) # inserting data entry into the collection


# making the tasks using the airflow python operator

# task1 is called API_call and utilises api_return_request function
task1 = PythonOperator(
    task_id = 'API_call',
    python_callable=api_return_request,
    dag=dag
)

# task2 is called to_mongo because it takes the return data from task1 and places the data in mongo Database
task2 = PythonOperator(
    task_id = 'to_mongo',
    python_callable=request_to_mongo,
    provide_context = True,
    op_kwargs={'meal_list': task1.output}, # tells airflow to take return value from function called in task1
    dag=dag
)

task2.set_upstream(task1) # making task2 dependant on task1's success

