# Apache Airflow MongoDB Pipeline

## Objectives

- break down python processes into functions so that a DAG can be made
- establish connection between MongoDB and local host on an airlfow container running in docker
- periodically make API request
- select relevant data from json response
- insert data into MongoDB

## Overview Diagram

![alt text](High-Level-Overview.png)
